var icon = document.getElementById("icon_mode")
icon.onclick = function() {
  document.body.classList.toggle("dark_theme")
  if (document.body.classList.contains("dark_theme")) {
    icon.innerHTML = ` <i class="gg-sun"></i>`
  }
  else {
    icon.innerHTML = ` <i class="gg-moon"></i>`
  }

}